import turtle
import time
import random
import math

from snake_config import *

board = turtle.Screen()
board.title("Snake")
board.setup(width=BOARD_WIDTH_PIXELS,height=BOARD_HEIGHT_PIXELS)
board.bgcolor("black")
board.tracer(0)

snake = turtle.Turtle()
snake.shape("square")
snake.color("red")
snake.penup()
snake.speed(0)
snake.goto(0,0)
snake.direction = "stop"

def calc_distance(source, target):
    distance = math.sqrt((source.xcor() - target.xcor())**2 + (source.ycor() - food.ycor())**2)
    return distance

def get_random_location():
    x = random.randint(-BOARD_WIDTH_PIXELS/2,BOARD_HEIGHT_PIXELS/2)
    y = random.randint(-BOARD_WIDTH_PIXELS/2,BOARD_HEIGHT_PIXELS/2)
    return x,y

def go_up():
    snake.direction = "up"
def go_down():
    snake.direction = "down"
def go_left():
    snake.direction = "left"
def go_right():
    snake.direction = "right"

def move(dir):
    if dir == "up":
        snake.sety(snake.ycor()+MIN_STEP_SIZE_PIXELS)
    if dir == "down":
        snake.sety(snake.ycor()-MIN_STEP_SIZE_PIXELS)
    if dir == "left":
        snake.setx(snake.xcor()-MIN_STEP_SIZE_PIXELS)
    if dir == "right":
        snake.setx(snake.xcor()+MIN_STEP_SIZE_PIXELS)

board.listen()
board.onkeypress(go_up,"w")
board.onkeypress(go_down,"s")
board.onkeypress(go_left,"a")
board.onkeypress(go_right,"d")

food = turtle.Turtle()
food.shape("circle")
food.color("green")
food.penup()
food.goto(get_random_location())

if __name__ == '__main__':
    while True:
        move(snake.direction)
        distance = calc_distance(snake, food)
        if distance < MIN_STEP_SIZE_PIXELS :
            food.goto(get_random_location())
        board.update()
        time.sleep(SNAKE_SPEED_SECONDS)

board.mainloop()

